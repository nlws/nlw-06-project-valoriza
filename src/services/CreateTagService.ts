import { getCustomRepository } from "typeorm";
import { TagsRepositories } from "../repositories/TagsRepositories";


class CreateTagService {
  async execute(name: string){
    const tagsRepository = getCustomRepository(TagsRepositories);

    if(!name) {
      throw new Error("Incorrect name!");
    }
    const TagAlreadyExists = await tagsRepository.findOne({
      name,
    });
    if(TagAlreadyExists) {
      throw new Error("Tag already exists!");
    }
    const Tag = tagsRepository.create({
      name,
    });

    await tagsRepository.save(Tag);
    return Tag;
  }

}

export { CreateTagService };